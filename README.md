# Introducuction

pyuserbase implements a substitute for Python virtual environments, like the Python 3 only [PEP405 venv](https://docs.python.org/3/library/venv.html) or legacy [virtualenv](https://virtualenv.pypa.io/en/stable/). It uses the [PEP370](https://www.python.org/dev/peps/pep-0370/) `PYTHONUSERBASE` environment variable mechanism to accomplish this. It outputs parameters suitable for the eval function of a shell, which allows using and switching between different Python user base directories.

The parameters outputted are:
`PYTHONUSERBASE`     is assigned the new user base directory,
`PATH`               an updated path, prepended with the new user scripts
                     directory so Python programs installed in the user base
                     directory will be found before any other versions installed
                     on the system,
`PYUSERBASE_OLDPATH` the original value of `PATH`,
`PYUSERBASE_NAME`    the name of the new user base, useful for shell prompt
                     decoration.


## Use cases

The problem of running different Python software projects which need different versions of the same library on a single system is traditionally solved using Python venvs (or one of the legacy variants). One of the characteristics of Python venvs is that the Python scripts from packages and venv helper scripts installed in such an environment are tied to the exact location in which they are installed.

pyuserbase does not use an activate script like the one created by venv. Also Python's distutils package (and by extension pip) does not use the venv installed python interpreter in the hashbang of installed script, when using the PEP370 mechanism. This means the environments created with pyuserbase can be easily relocated.

One of the issues the use of pyuserbase can solve is easy relocation of a larger number of Python code protects to a different location on disk. This normally would mean recreating any venv included in one of the code protects. Another use case is simplistic deployments of a Python project on a machine that does not have access to the outside world and thus PyPI. In this case it is useful to be able to pick up a virtual environment created on a different machine and copy it to the deployment machine. But if the deployment machine does not allow for the same path to be used, a traditional venv would not work.

Although pyuserbase is written in Python 3, it can be used just as easily to setup Python 2 environments.


# Installation

The functioning of pyuserbase depends on the setting and changing of the `PYTHONUSERBASE` environment variable, installing pyuserbase as a user package (`pip install --user`) is thus not recommended. As soon as a Python user base different from the default user base is set up, the script would not be functional anymore. Unless of course the new user base itself also has pyuserbase installed. Therefore installing pyuserbase globally is the recommended way.

Globally installing python packages is usually surrounded by warnings and scary stories of wrecking the Python environment set up by the Linux distribution used. This is generally the case for Linux distributions that do not provision for a way to safely install packages from PyPI next to the distribution provided packages, or for not using the distribution provided version of pip/distutils (as is for example needed for Debian and its derived distributions). The latter is not surprising, as pip is very adamant on being updated to the latest version. Since this newer version is downloaded from PyPI it does not include the Linux distribution specific provisions to allow for safely installing non-distribution provided Python packages.


## Shell integration

pyuserbase does not install any scripts in the user base environment it sets up, so a different means of activating and deactivating the different user bases is needed. This is accomplished by simple integration into the used shell.

Add the following line somewhere in your `.bashrc`, `.kshrc` or `.zshrc`:

```
function pyb { eval $(pyuserbase $1); }
```

Now, when logging in anew, you can type `pyb <name of user base>` and start working with the user base named.


### Shell prompt decoration

Another useful feature to have is some indication that a specific Python user base is used. To decorate your shell prompt with the name of the Python user base, add the following to the end of your `.bashrc` or `.ksshrc`:

```
PS1='${PYUSERBASE_NAME:+($PYUSERBASE_NAME)}'$PS1
```

# Usage

> Note that all examples assume the shell integration explained in the _Installation_ section is in place.

The usage of pyuserbase is very straightforward. It either accepts an option or the name of a user base to activate. The options avaialable are `-h` or `--help` for displaying a simple help text, or `--off` to turn off the currently active user base (and thus revert to the default user base).

The `--off` option can be used even when no non-default user base is active. The option just resets the shell environment variables `PYTHONUSERBASE`, `PYUSERBASE_NAME` and `PYUSERBASE_OLDPATH` to an empty value and reverts the `PATH` variable to its former value (in case no user base is active, the `PATH` variable is left as is).

The name of a user base to activate may be anything except the values `-h`, `--help` or `--off`. When activating a previously non existing user base, no directories are created yet. They will be when using `pip --user` or `python setup.py --user` for the first time.

If a user base name does not contain any directory parts, like:

```
$ pyb my-base
```

the Python user base directory is set to the `my-base` directory under the default pyuserbase location `$HOME/.local/pyuserbase`. This location was chosen to be in line with the default Python user base, which is located under `$HOME/.local`.

However if the user base name does contain any directory parts, like:

```
$ pyb ./my-base
```

or

```
$ pyb /tmp/my-base
```

the python user base directory is set to the `my-base` directory in the current working directory or to `/tmp/mybase` respectively.


## Using a pyuserbase environment

The following is a simple example on how to use pyuserbase, provided you set it up according to the *Installation* section. First setup a new user base:

```
$ pyb my-new-userbase`
```

Then install for example a package from PyPI:

```
(my-new-userbase)$ pip install --user <pypi-pkg>`

```

> Note that when installing packages from PyPI, the `--user` option somehow does not need to be specified when running as a non-root user.

Next install some local Python wheel:

```
(my-new-user-base)$ pip install --user <file.whl>
```

> Note that the use of `--user` here is very much not optional.

And finally install some local project in development mode:

```
(my-new-user-base)$ python3 ./setup.py develop --user
```

## Global packages

The PEP370 mechanism of local, user installed packages has the property that all globally installed packages are still available when using a (different) Python user base. This has both benefits and disadvantages. A benefit is that any dependencies already fulfilled by the distribution installed Python environment do not need to be installed a second time (given the installed versions of packages are sufficient), leading to much smaller user base directories. A disadvantage could be that installed packages may differ between machines when using the Python user base mechanism as simple deployment tool.

To force the installation of a Python package that is already installed globally use the `-I` option:

```
(my-new-userbase)$ pip install --user -I <pypi-pkg>`

```

This works for both the single package being installed as well as all its dependencies.


# Portability of environments to other systems

Moving a Python user base to a different machine is as simple as creating an archive of a specific user base directory and extracting it again on the target machine. The location of the directories may be different on originating and target machines. If the directory on the target machine is not under `$HOME/.local/pyuserbase` however, make sure to use the full directory name when activating the user base.

The PEP370 mechanism keeps all globally installed Python packages available in the Python user base, packages installed in the user base itself will override the globally installed ones. When moving an environment to another system, make sure all packages and package versions that are depended upon are either also globally available on the target machine or are installed in the user base that is moved itself.


# Known issues

The most obvious issue is that, when installed as user, pyuserbase will not work once a different Python user base is activated. This is something inherent to the way the PEP370 mechanism works. Since globally installed packages are still available and pyuserbase has no other dependencies than Python itself, there is really no reason not to install it globally anyway.

During testing on Ubuntu 16.04 some weird caching of binary locations was observed. The specific case observed was with installing an updated pip in a user base using the distribution provided pip. After installing the new pip, executing it would still execute the original distribution version of pip. But when switching to a different terminal and activating the pyuserbase the correct pip was found.
