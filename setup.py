import setuptools


setuptools.setup(
    name='pyuserbase',
    version='0.2.0',
    url='https://gitlab.com/pyrco/pyuserbase',
    author='Jan Willem Brandenburg',
    author_email='pyuserbase@code.mailstack.nl',
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: POSIX",
        "Operating System :: MacOS :: MacOS X",
    ),
    license='GNU General Public License v3 or later (GPLv3+)',
    description="""Lightweight substitute for python virtual envs using the \
PEP370 PYTHONUSERBASE environment variable mechanism.""",
    long_description="""pyuserbase implements a lightweight substitute for
python virtual envs using the PEP370 PYTHONUSERBASE environment variable
mechanism. It outputs paramters, suitable for the eval function of a shell,
which allow using and switching different python user base directories.""",
    entry_points={
        'console_scripts': [
             'pyuserbase = pyuserbase.pyuserbase:main',
        ],
    },
    packages=setuptools.find_packages(),
)
