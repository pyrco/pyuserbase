# Copyright © 2018 Jan Willem Brandenburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""pyuserbase implements a lightweight substitute for python virtualenvs using
the PEP370 PYTHONUSERBASE environment variable mechanism. It outputs
parameters suitable for the eval function of a shell, which allow using and
switching between different python user base directories.

The parameters outputted are:
PYTHONUSERBASE     is assigned the new user base directory,
PATH               an updated path, prepended with the new user scripts
                   directory so python programs installed in the user base
                   directory will be found before any other versions installed
                   on the system,
PYUSERBASE_OLDPATH the original value of PATH,
PYUSERBASE_NAME    the name of the new user base, useful for shell prompt
                   decoration.
"""

import os
import sys


PYUSERBASE_LOCATION = ".local/pyuserbase"
ENV_HOME = os.environ.get("HOME", "/")
ENV_PATH = os.environ.get("PATH", "")
ENV_PYUSERBASE_OLDPATH = os.environ.get("PYUSERBASE_OLDPATH", "")


def usage():
    """Print a usage mesage for this program."""
    usage_msg = """Usage: {} OPTION | BASENAME
          
-h, --help  display this help and exit
--off       output the env vars to turn off using the python user base
            directory
BASENAME    output the env vars to use BASENAME as the new python user base
            directory

pyuserbase implements a lightweight substitute for python virtualenvs using the
PEP370 PYTHONUSERBASE environment variable mechanism. It outputs parameters
suitable for the eval function of a shell, which allows using and switching
between different python user base directories.

To switch to a different python user base, it is not needed to first turn off
the previous python user base.

If BASENAME does not contain any directory elements, the python user base is
located in the default pyuserbase directory under $HOME/{}

pyuserbase  Copyright © 2018 Jan Willem Brandenburg
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law."""
    usage_msg = usage_msg.format(os.path.basename(sys.argv[0]),
                                 PYUSERBASE_LOCATION)
    warning(usage_msg)


def warning(msg):
    """Print msg to stderr, output to stderr will not be evaluated by a shell's
    eval function.
    """
    # msg may contain surrogateescaped commandline parameter values. Since
    # print does not allow for setting a different encoding error handler, we
    # need to do the outputting of msg in a somewhat roundabout way.
    msg += "\n"
    msg = bytes(msg, sys.stderr.encoding, "surrogateescape")
    sys.stderr.buffer.write(msg)


def output_env(env_vars):
    """Output the keys and values in the env_vars dict as shell variable
    assignments suitable to be eval'ed by a shell's eval function.
    """
    #FEATURE: output shell specific commands (e.g. csh like shells use setenv)
    for name, value in env_vars.items():
        # The shell is expecting all variables containing paths to be the exact
        # bytes they are on disk irrepective of the actual encoding used for
        # the filesystem.
        # When reading environment variables or commandline parameters (on
        # non-Windows systems) the values are decoded with the
        # 'surrogateescape' error handler to handle byte values invalid for the
        # local filesystem encoding, in this way they can be preserved.
        # Since print uses sys.stout without the option to set a decoder or decode
        # error handler, we need to do it in a somewhat roundabout way, to be able to
        # output the exact bytes.
        line = "{}='{}'; export {};\n".format(name, value, name)
        line = os.fsencode(line)
        sys.stdout.buffer.write(line)


def pyuserbase_on(user_base_name):
    """Ouput parameters for a shell to be eval'ed that allow user_base_name to
    be used as the new user base directory.

    PYTHONUSERBASE      is assigned the new user base directory,
    PATH                gets the new user scripts directory prepended so
                        python scripts installed in the user base directory
                        will be found before any other versions installed on
                        the system,
    PYUSERBASE_OLDPATH  is assigned the original value for PATH, this allows
                        for cleanup parameters to be outputted by this script
                        using the pyuserbase_off() funtion,
    PYUSERBASE_NAME     is assigned the value of basename, useful for shell
                        prompt decoration.

    If PYUSERBASE_OLDPATH is already set to a value, the value used for PATH is
    based on this and not the current value of PATH, PYUSERBASE_OLDPATH will in
    this case be left as is.  This prevents the continuous appending of paths
    to PATH when switching from pyuserbase to pyuserbase.
    """
    path, base_name = os.path.split(user_base_name)
    if not path:
        pyuserbase_dir = os.path.join(ENV_HOME, PYUSERBASE_LOCATION)
        user_base_dir = os.path.join(pyuserbase_dir, base_name)
    else:
        user_base_dir = os.path.abspath(user_base_name)
    if not os.path.exists(user_base_dir):
        warning("""The '{}' python user base does not yet exist (using pip to \
install packages will automatically create it).""".format(user_base_name))
    user_script_dir = os.path.join(user_base_dir, "bin")

    oldpath = ENV_PYUSERBASE_OLDPATH
    if oldpath:
	# switching from one pyuserbase to another, PYUSERBASE_OLDPATH should
	# be the original 'clean' path without any pyuserbase directories
	# prepended to it.
        path = oldpath
    else:
        path = ENV_PATH
        oldpath = path
    path = "{}:{}".format(user_script_dir, path)

    new_env = {"PYTHONUSERBASE": user_base_dir,
               "PYUSERBASE_NAME": base_name,
               "PYUSERBASE_OLDPATH": oldpath,
               "PATH": path,
              }
    return new_env


def pyuserbase_off():
    """Ouput parameters for a shell to be eval'ed that allow for the current
    user base directory to be disabled.

    PATH gets assigned the value of PYUSERBASE_OLDPATH, resetting it to the
    original path. If PYUSERBASE_OLDPATH does not exist, its value is left as is.
    PYTHONUSERBASE, PYUSERBASE_OLDPATH and PYUSERBASE_NAME are always set to an empty
    string.
    """
    if ENV_PYUSERBASE_OLDPATH:
        path = ENV_PYUSERBASE_OLDPATH
    else:
        warning("""Currently no python user base seems to be in use, forcing \
environment cleanup anyway.""")
        path = ENV_PATH

    new_env = {"PYTHONUSERBASE": "",
               "PYUSERBASE_NAME": "",
               "PYUSERBASE_OLDPATH": "",
               "PATH": path,
              }
    return new_env


def main():
    """The main entry point for this script."""
    if len(sys.argv) != 2:
        warning("Wrong number of paramaters specified, expected only 1")
        usage()
        sys.exit(1)
    opt = sys.argv[1]
    if opt in ("-h", "--help"):
        usage()
        sys.exit(0)
    else:
        if opt == "--off":
            new_env = pyuserbase_off()
        else:
            new_env = pyuserbase_on(opt)
        output_env(new_env)


if __name__ == "__main__":
    sys.exit(main())
